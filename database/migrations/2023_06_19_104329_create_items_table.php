<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('date_added');
            $table->string('code');
            $table->string('brand');
            $table->string('model');
            $table->boolean('is_discarded')->nullable()->default(false);
            $table->string('discard_reason')->nullable();
            $table->boolean('is_defective')->nullable()->default(false);
            $table->string('defect_reason')->nullable();
            $table->text('description');
            $table->unsignedInteger('equipment_id');
            $table->foreign('equipment_id')
                ->references('id')
                ->on('equipment')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
