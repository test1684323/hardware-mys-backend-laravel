<?php

namespace App\Traits;

trait ApiResponserTrait
{

    protected function validationError($validation)
    {
        return response()->json(['validation' => $validation->errors()], 422);
    }

    protected function error($message, $code = 404)
    {
        return response()->json(['error' => $message], $code);
    }

    protected function success($message, $code = 200)
    {
        return response()->json(['message' => $message], $code);
    }

    protected function data($value, $code = 200)
    {
        return response()->json($value, $code);
    }
}
