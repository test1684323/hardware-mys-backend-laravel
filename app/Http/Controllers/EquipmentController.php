<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Traits\ApiResponserTrait;

class EquipmentController extends Controller
{
    use ApiResponserTrait;

    private function equipment($id)
    {
        $value = Equipment::with('items')->find($id);
        return $value != null ? $value : false;
    }

    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        $equipment = Equipment::orderBy('id', 'desc')->get();
        return $this->data(['equipment' => $equipment]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|max: 30|unique:equipment,name',
        ]);

        if ($validation->fails()) {
            return $this->validationError($validation);
        }

        $equipment = new Equipment();
        $equipment->name = $request->name;
        $equipment->slug = Str::slug($request->name);

        $equipment->save();

        return $this->success('Equipment Created!', 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        $equipment = $this->equipment($request->id);
        if (!$equipment) {
            return $this->error('Equipment Not Found!');
        }

        return $this->data(['equipment' => $equipment]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $equipment = $this->equipment($request->id);
        if (!$equipment) {
            return $this->error('Equipment Not Found!');
        }

        $validation = Validator::make($request->all(), [
            'name' => 'required|max: 30|unique:equipment,name,' . $request->id,
        ]);

        if ($validation->fails()) {
            return $this->validationError($validation);
        }

        $equipment->name = $request->name;
        $equipment->slug = Str::slug($request->name);

        $equipment->save();

        return $this->success('Equipment Updated!', 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $equipment = $this->equipment($request->id);
        if (!$equipment) {
            return $this->error('Equipment Not Found!');
        }
        //return $equipment;
        // $equipment->items()->delete();
        $equipment->delete();

        return $this->success('Equipment Deleted!', 200);
    }
}
