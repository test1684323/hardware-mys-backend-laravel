<?php

use App\Http\Controllers\EquipmentController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemUserController;
use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('users')
    ->controller(UserController::class)
    ->group(function () {
        Route::get('/', 'index');
        Route::get('/{id}', 'show');
        Route::get('/{id}/profile/', 'profile');
        Route::delete('/{id}', 'destroy');
    });

Route::prefix('equipment')
    ->controller(EquipmentController::class)
    ->group(function () {
        Route::get('/', 'index')->name('all');
        Route::get('/{id}', 'show')->name('single');
        Route::post('/', 'store')->name('save');
        Route::patch('/{id}', 'update')->name('update');
        Route::delete('/{id}', 'destroy')->name('delete');
    });

Route::prefix('items')
    ->controller(ItemController::class)
    ->group(function () {
        Route::get('/', 'index')->name('all');
        Route::get('/{id}', 'show')->name('single');
        Route::post('/', 'store')->name('save');
        Route::patch('/{id}', 'update')->name('update');
        Route::delete('/{id}', 'destroy')->name('delete');
        Route::get('/search/{code}', 'search')->name('search');
    });

Route::prefix('item-user')
    ->controller(ItemUserController::class)
    ->group(function () {
        Route::post('/assign', 'assign')->name('assign');
        Route::post('/restock', 'restock')->name('restock');
    });
