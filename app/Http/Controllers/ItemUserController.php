<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;

class ItemUserController extends Controller
{

    use ApiResponserTrait;

    public function index(Request $request)
    {

    }

    public function assign(Request $request){
        $user = User::find($request->user_id);
        $user->items()->attach($request->item_id);
        return  $this->success('Item Assigned');
    }

    public function restock(Request $request){
        $user = User::find($request->user_id);
        $user->items()->detach($request->item_id);
        return  $this->success('Item Restocked');
    }


    public function destroy(Request $request)
    {
        //
    }
}
