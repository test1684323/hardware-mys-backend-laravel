<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
    ];

    protected $appends = ['label'];

    public function name(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => ucwords($value)
        );
    }

    public function label(): Attribute
    {
        return Attribute::make(
            get: fn () => ucwords($this->attributes['name'])
        );
    }

    public function items()
    {
        return $this->hasMany(Item::class, 'equipment_id', 'id');
    }
}
