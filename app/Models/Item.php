<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'date_added',
        'code',
        'brand',
        'model',
        'is_discarded',
        'discard_reason',
        'is_defective',
        'defect_reason',
        'description',
        'equipment_id',
        'equipment_id'
    ];

    public function equipment()
    {
        return $this->belongsTo(Equipment::class, 'equipment_id', 'id')->with('equipment');
    }

    public function users(){
        return $this->belongsToMany(User::class,
        'item_user',
        'item_id',
        'user_id',
        'id',
        'id')
        ->withPivot(['id','is_restocked'])
        ->withTimestamps();
    }
}
