<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponserTrait;

class ItemController extends Controller
{

    use ApiResponserTrait;

    private function item($id)
    {
        $value = Item::find($id);
        return $value != null ? $value : false;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = Item::orderBy('id', 'desc')->get();
        return $this->data(['items' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'date_added' => 'required',
            'code' => 'required|numeric|unique:items,code',
            'brand' => 'required|max:30',
            'model' => 'required|max:50',
            'is_discarded' => 'nullable|between:0,1',
            'discard_reason' => 'required_if:is_discarded, 1',
            'is_defective' => 'nullable|between:0,1',
            'defect_reason' => 'required_if:is_defective, 1',
            'description' => 'required',
            'equipment_id' => 'required|numeric'
        ]);

        if ($validation->fails()) {
            return $this->validationError($validation);
        }

        $item = new Item();
        $item->fill($request->all());
        $item->save();

        return $this->success('Item Created!', 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        $item = $this->item($request->id);
        if (!$item) {
            return $this->error('Item Not Found');
        }

        return $this->data(['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $item = $this->item($request->id);
        if (!$item) {
            return $this->error('Item Not Found');
        }

        $validation = Validator::make($request->all(), [
            'date_added' => 'required',
            'code' => 'numeric|unique:items,code,' . $request->id,
            'brand' => 'required|max:30',
            'model' => 'required|max:50',
            'is_discarded' => 'nullable|between:0,1',
            'discard_reason' => 'required_if:is_discarded, 1',
            'is_defective' => 'nullable|between:0,1',
            'defect_reason' => 'required_if:is_defective, 1',
            'description' => 'required',
            'equipment_id' => 'required|numeric'
        ]);

        if ($validation->fails()) {
            return $this->validationError($validation);
        }

        $item->fill($request->all());

        if ($request->has('is_discarded') && $request->is_discarded == 0) {
            $item->discard_reason = null;
        }

        if ($request->has('is_defective') && $request->is_defective == 0) {
            $item->defect_reason = null;
        }

        $item->save();

        if ($item->wasChanged() == false) {
            return $this->success('No Change');
        }

        return $this->success('Item Updated!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $item = $this->item($request->id);
        if (!$item) {
            return $this->error('Item Not Found');
        }
        //return $item->users;
        //$item->users()->delete();
        $item->delete();

        return $this->success('Item Deleted!');
    }

    public function search(Request $request){
        $items = Item::select('items.*', 'item_user.is_restocked')
        ->leftJoin('item_user', function ($join) {
            $join->on('items.id', '=', 'item_user.item_id');
        })
        ->where('code', 'like', '%'.$request->code.'%')
        ->where(function ($query) {
            $query->whereNull('is_restocked')
                ->orWhere('is_restocked', 1);
        })
        ->get();

    return $items;
    }
}
