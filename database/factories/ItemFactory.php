<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Equipment;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    public function definition(): array
    {
        $equipment = Equipment::inRandomOrder()->first();
        return [
            'date_added' => '2023/01/02',
            'code' => mt_rand(101, 999),
            'brand' => fake()->word,
            'model' => fake()->word,
            'description' => fake()->sentence(6),
            'equipment_id' => $equipment->id
        ];
    }
}
