<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use ApiResponserTrait;

    private function user($id)
    {
        $value = User::find($id);
        return $value != null ? $value : false;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $users = User::orderBy('name', 'ASC')->get();
        return $this->data(['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        $user = $this->user($request->id);
        if (!$user) {
            return $this->error('User Not Found!');
        }

        return $this->data(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $user = $this->user($request->id);
        if (!$user) {
            return $this->error('User Not Found!');
        }

        $user->delete();

        return $this->success('User is Deleted');
    }

    public function profile(Request $request){

        $user = User::with(['items' => function ($query) {
            $query->wherePivot('is_restocked', 0);
        }])
        ->find($request->id);

        return $this->data(['user'=>$user]);

    }
}
